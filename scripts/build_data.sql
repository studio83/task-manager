INSERT INTO task_manager.`user` (user_id, email, password, create_time, name, surname, is_admin) VALUES(1, 'wojtishek@gmail.com', '$2y$10$x0jI6c9XxH5JFLPzu7OEG.jvv5U3hfkcRasy4GacqJgS3cVucqwFO', '2020-03-31 11:03:14', 'Vojtěch', 'Kaizr', 1);

INSERT INTO task_manager.status (status_id, title, color) VALUES(1, 'Nový', '#4263ff');
INSERT INTO task_manager.status (status_id, title, color) VALUES(2, 'Ke zpracování', '#fffc68');
INSERT INTO task_manager.status (status_id, title, color) VALUES(3, 'K odevzdání', '#ff905f');
INSERT INTO task_manager.status (status_id, title, color) VALUES(4, 'Hotový', '#7cff80');
