<?php

declare(strict_types=1);

namespace App\Model;

use Nette;
use Nette\Database\Context;
use Nette\Security as NS;


/**
 * Class UserModel
 * @package App\Model
 */
class UserModel {

	/**
	 * @var Context
	 */
	private $database;
	/**
	 * @var NS\User
	 */
	private $user;

	/**
	 * UserModel constructor.
	 * @param Context $database
	 * @param NS\User $user
	 */
	public function __construct(Context $database, NS\User $user)
	{
		$this->database = $database;
		$this->user = $user;
	}


	/**
	 * @return array|Nette\Database\Table\IRow[]
	 */

	public function getUsers()
	{
		return $this->database->table('user')->fetchAll();
	}


	/**
	 * @param int $id ID uživatele
	 * @return Nette\Database\Table\ActiveRow|null
	 * @throws NS\AuthenticationException
	 */

	public function getUser($id)
	{
		if(!$this->user->isInRole('sa')) {
			throw new NS\AuthenticationException('Nemáte oprávnění k zobrazení tohoto uživatele');
		}
		return $this->database->table('user')->get($id);

	}


	/**
	 * @param int   $id       ID uživatele
	 * @param array $userData Pole informací o uživateli
	 * @throws NS\AuthenticationException
	 */

	public function updateUser($id, $userData) {
		if(!$this->user->isInRole('sa')) {
			throw new NS\AuthenticationException('Nemáte oprávnění k úpravě uživatele');
		}
		$userRow = $this->getUser($id);
		if(!$userRow) {
			throw new NS\AuthenticationException('Tento uživatel neexistuje');
		}
		$userRow->update([
			'name' => $userData['name'],
			'surname' => $userData['surname'],
			'email' => $userData['email']
		]);
	}


	/**
	 * @param int $id ID uživatele
	 * @throws NS\AuthenticationException
	 */

	public function deleteUser($id)
	{
		if(!$this->user->isInRole('sa')) {
			throw new NS\AuthenticationException('Nemáte oprávnění k mazání uživatele');
		}
		$userRow = $this->getUser($id);
		if(!$userRow) {
			throw new NS\AuthenticationException('Tento uživatel neexistuje');
		}
		if($this->user->getId() === $userRow->getPrimary()) {
			throw new NS\AuthenticationException('Nelze smazat svůj vlastní profil');
		}
		$userRow->delete();
	}


	/**
	 * @return array|Nette\Database\Table\IRow[]
	 */

	public function getGroups() {
		return $this->database->table('group')->fetchAll();
	}


	/**
	 * @param int $id
	 * @return Nette\Database\Table\ActiveRow|null
	 */
	public function getGroup($id)
	{
		return $this->database->table('group')->get($id);
	}


	/**
	 * @param array $data
	 * @return bool|int|Nette\Database\Table\ActiveRow
	 * @throws NS\AuthenticationException
	 */
	public function addGroup($data)
	{
		$addGroup = $this->database->table('group')->insert([
			'name' => $data['name']
		]);
		if(!$addGroup) {
			throw new NS\AuthenticationException('Nepodařilo se přidat novou skupinu');
		}
		return $addGroup;
	}
}