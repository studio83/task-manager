<?php

declare(strict_types=1);

namespace App\Model;

use Nette;
use Nette\Database\Context;
use Nette\Security as NS;
use App\Model\AuthenticatorModel;


/**
 * Class RegistrationModel
 * @package App\Model
 */
class RegistrationModel {

	/**
	 * @var NS\Passwords
	 */
	private $passwords;
	/**
	 * @var Context
	 */
	private $database;
	/**
	 * @var \App\Model\AuthenticatorModel
	 */
	private $authenticator;

	/**
	 * RegistrationModel constructor.
	 * @param NS\Passwords                  $passwords
	 * @param Context                       $database
	 * @param \App\Model\AuthenticatorModel $authenticator
	 */
	public function __construct(NS\Passwords $passwords, Context $database, AuthenticatorModel $authenticator) {
		$this->passwords = $passwords;
		$this->database = $database;
		$this->authenticator = $authenticator;
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @param array $userData
	 * @throws NS\AuthenticationException
	 */
	public function register($username, $password, $userData = []) {
		if($this->database->table('user')->where('email', strtolower($username))->fetch()) {
			throw new NS\AuthenticationException('Tento uživatel již existuje');
		}

		$this->database->table('user')->insert([
			'email' => $username,
			'password' => $this->passwords->hash($password),
			'name' => $userData['name'],
			'surname' => $userData['surname'],
			'is_admin' => !empty($userData['is_admin']) ? 1 : 0
		]);

		$credentials = [$username, $password];

		$this->authenticator->authenticate($credentials);
	}

}