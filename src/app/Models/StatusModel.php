<?php

declare(strict_types=1);

namespace App\Model;

use Nette\Database\Context;
use Nette\Security as NS;

/**
 * Class StatusModel
 */
class StatusModel {

	/**
	 * @var Context
	 */
	private $database;

	/**
	 * @var NS\User
	 */
	private $user;

	/**
	 * StatusModel constructor.
	 * @param Context $database
	 */
	public function __construct(Context $database, NS\User $user)
	{
		$this->database = $database;
		$this->user = $user;
	}

	/**
	 * @return array|\Nette\Database\Table\IRow[]
	 */
	public function getStatuses() {
		return $this->database->table('status')->fetchAll();
	}

	/**
	 * @param array $data
	 * @throws NS\AuthenticationException
	 */
	public function saveStatus($data)
	{
		if(!$this->user->isInRole('sa')) {
			throw new NS\AuthenticationException('Nemáte oprávnění ke změně stavu úkolu');
		}
		$this->database->table('status')->insert([
			'title' => $data['title'],
			'color' => $data['color'] ?: 'aaaaff'
		]);
	}

	/**
	 * @param int $id
	 * @throws NS\AuthenticationException
	 */
	public function deleteStatus($id)
	{
		if(!$this->user->isInRole('sa')) {
			throw new NS\AuthenticationException('Nemáte oprávnění ke smazání stavu úkolu');
		}
		$status = $this->database->table('status')->get($id);
		if(!$status) {
			throw new NS\AuthenticationException('Tento stav úkolu neexistuje');
		}
		$status->delete();
	}
}