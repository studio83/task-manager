<?php

declare(strict_types=1);

namespace App\Model;

use Nette\Security as NS;
use Nette\Database\Context;


/**
 * Class ProfileModel
 * @package App\Model
 */
class ProfileModel {

	/**
	 * @var NS\User
	 */
	private $user;
	/**
	 * @var Context
	 */
	private $database;
	/**
	 * @var NS\Passwords
	 */
	private $passwords;

	/**
	 * ProfileModel constructor.
	 * @param NS\User      $user
	 * @param Context      $database
	 * @param NS\Passwords $passwords
	 */
	public function __construct(NS\User $user, Context $database, NS\Passwords $passwords)
	{
		$this->user = $user;
		$this->database = $database;
		$this->passwords = $passwords;
	}


	/**
	 * @param string $emailAddress
	 * @param array $userData
	 * @throws NS\AuthenticationException
	 */
	public function updateProfile($emailAddress, $userData)
	{
		$profile = $this->database->table('user')->where('email', $emailAddress)->fetch();
		if(!$profile) {
			throw new NS\AuthenticationException('Tento uživatel neexistuje');
		}
		if($this->database->table('user')->where('email', $userData['email'])->where('user_id NOT', [$this->user->getId()])->fetch()) {
			throw new NS\AuthenticationException('Uživatel s adresou '.$userData['email'].' již existuje.');
		}
		$update = $this->database->table('user')->update([
			'email' => strtolower($userData['email']),
			'name' => $userData['name'],
			'surname' => $userData['surname']
		]);

		$this->user->getIdentity()->name = $userData['name'];
		$this->user->getIdentity()->surname = $userData['surname'];
		$this->user->getIdentity()->email = strtolower($userData['email']);
	}

	/**
	 * @param string $current Aktuální heslo
	 * @param string $new Nové heslo
	 * @throws NS\AuthenticationException
	 */

	public function updatePassword($current, $new)
	{
		$currentUserEmail = $this->user->getIdentity()->email;
		$checkPassword = $this->database->table('user')->where('email', $currentUserEmail)->fetch();
		if(!$checkPassword) {
			throw new NS\AuthenticationException('Tento uživatel neexistuje');
		}
		if(!$this->passwords->verify($current, (string) $checkPassword->password)) {
			throw new NS\AuthenticationException('Původní heslo nesouhlasí');
		}
		$this->database->table('user')->get($this->user->getId())->update([
			'password' => $this->passwords->hash($new)
		]);
	}
}