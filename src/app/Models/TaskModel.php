<?php

declare(strict_types=1);

namespace App\Model;

use Nette;
use Nette\Database\Context;
use Nette\Security as NS;


/**
 * Class TaskModel
 * @package App\Model
 */

class TaskModel {

	/**
	 * @var Context
	 */
	private $database;


	/**
	 * @var NS\User
	 */
	private $user;

	/**
	 * TaskModel constructor.
	 * @param Context $database
	 */

	public function __construct(Context $database, NS\User $user)
	{
		$this->database = $database;
		$this->user = $user;
	}

	/**
	 * @param int $id
	 * @return Nette\Database\Table\ActiveRow|null
	 */
	public function getTask($id) {
		return $this->database->table('task')->get($id);
	}


	/**
	 * @param int $id ID uživatele
	 * @return array
	 */
	public function getTasksByUser($id)
	{
		$tasks = [];
		$taskUser = $this->database->table('user_task')->where('user_user_id', $id)->fetchAll();
		foreach($taskUser as $row) {
			$tasks[$row->getPrimary()] = $this->getTask($row->task_task_id);
		}
		return $tasks;
	}

	/**
	 * @param array $data
	 * @throws NS\AuthenticationException
	 */
	public function addMyTask($data, $group = 0) {
		$task = $this->database->table('task')->insert($data);
		if(!$task) {
			throw new NS\AuthenticationException('Nepodařilo se přidat nový úkol.');
		}

		$userTask = $this->database->table('user_task')->insert([
			'user_user_id' => $this->user->getId(),
			'task_task_id' => $task->getPrimary()
		]);
		if(!$userTask) {
			$task->delete();
			throw new NS\AuthenticationException('Nepodařilo se přiřadit uživatele / skupinu k úkolu');
		}

		if($group !== 0) {
			$groupTask = $this->database->table('group_task')->insert([
				'group_group_id' => $group,
				'task_task_id' => $task->getPrimary()
			]);
			if(!$groupTask) {
				throw new NS\AuthenticationException('Nepodařilo se přiřadit úkol ke skupině');
			}
		}
	}

}