<?php

declare(strict_types=1);

namespace App\Model;

use Nette;
use Nette\Database\Context;
use Nette\Security as NS;
use App\Model\UserModel;
use function Sodium\add;


/**
 * Class GroupModel
 * @package App\Model
 */
class GroupModel {

	/**
	 * @var Context
	 */
	private $database;
	/**
	 * @var NS\User
	 */
	private $user;

	/**
	 * @var \App\Model\UserModel
	 */
	private $userModel;


	/**
	 * GroupModel constructor.
	 * @param Context              $database
	 * @param NS\User              $user
	 * @param \App\Model\UserModel $userModel
	 */
	public function __construct(Context $database, NS\User $user, UserModel $userModel)
	{
		$this->database = $database;
		$this->user = $user;
		$this->userModel = $userModel;
	}

	/**
	 * @return array
	 */
	public function getMyGroups() {
		$groups = [];
		$userGroups = $this->database->table('user_group')->where('user_user_id', $this->user->getId())->fetchAll();
		foreach($userGroups as $row) {
			$groups[] = $this->userModel->getGroup($row->group_group_id);
		}
		return $groups;
	}

	/**
	 * @param array $data
	 * @throws NS\AuthenticationException
	 */
	public function addMyGroup($data)
	{
		$addMyGroup = $this->userModel->addGroup($data);
		$addGroupToMyself = $this->database->table('user_group')->insert([
			'user_user_id' => $this->user->getId(),
			'group_group_id' => $addMyGroup->getPrimary()
		]);
		if(!$addGroupToMyself) {
			$addMyGroup->delete();
			throw new NS\AuthenticationException('Nepodařilo se přiřadit skupinu k uživateli');
		}
	}

}