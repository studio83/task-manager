<?php

declare(strict_types=1);

namespace App\Model;

use Nette\Security as NS;
use Nette\Database\Context;


/**
 * Class AuthenticatorModel
 * @package App\Model
 */
class AuthenticatorModel implements NS\IAuthenticator
{
	/**
	 * @var Context
	 */
	private $database;
	/**
	 * @var NS\Passwords
	 */
	private $passwords;

	/**
	 * AuthenticatorModel constructor.
	 * @param Context      $database
	 * @param NS\Passwords $passwords
	 */
	public function __construct(Context $database, NS\Passwords $passwords) {
		$this->database = $database;
		$this->passwords = $passwords;
	}

	/**
	 * @param array $credentials
	 * @return NS\IIdentity
	 * @throws NS\AuthenticationException
	 */
	public function authenticate(array $credentials): NS\IIdentity
	{
		[$username, $password] = $credentials;
		$login = $this->database->table('user')->where('email', $username)->fetch();

		if(!$login) {
			throw new NS\AuthenticationException('Uživatel s touto e-mailovou adresou nebyl nalezen', self::IDENTITY_NOT_FOUND);
		}

		if(!$this->passwords->verify((string) $password, (string) $login->password)) {
			throw new NS\AuthenticationException('Nesprávné přihlašovaní údaje', self::INVALID_CREDENTIAL);
		}

		$userData = [
			'name' => $login->name,
			'surname' => $login->surname,
			'email' => $login->email
		];
		return new NS\Identity($login->getPrimary(), $login->is_admin ? 'sa' : 'user', $userData);
	}

}