<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Security as NS;
use Nette\Mail as Mail;
use Nette\Application\UI;


/**
 * Class BasePresenter
 * @package App\Presenters
 */
class BasePresenter extends Nette\Application\UI\Presenter {

	/**
	 * @var NS\User
	 */
	private $user;

	/**
	 * BasePresenter constructor.
	 * @param NS\User $user
	 */
	public function __construct(NS\User $user) {
		$this->user = $user;
	}

	public function beforeRender()
	{
		parent::beforeRender();
		if($this->user && $this->user->isLoggedIn()) {
			$this->template->userData = $this->user->getIdentity()->data;
		}
	}

	public function renderDefault()
	{

	}

	/**
	 * @param string $emailAddress
	 * @param string $subject
	 * @param string $body
	 */
	public function sendEmail($emailAddress, $subject = '', $body = '')
	{
		try {
			$mail = new Mail\Message;
			$mail->setFrom('Task manager <dummy@taskmanager.none>')->addTo($emailAddress)->setSubject($subject)->setBody($body);
			$mail->setHeader('MIME-Version', '1.0');
			$mail->setHeader('Content-Type', 'text/html; charset=utf-8');

			$mailer = new Mail\SendmailMailer;
			$mailer->send($mail);
		} catch(Mail\SendException $e) {
			$this->flashMessage($e->getMessage(), 'danger');
		}
	}

	/**
	 * @throws Nette\Application\AbortException
	 */
	public function checkLogin()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->flashMessage('Pro přístup do aplikace je potřeba se přihlásit.', 'warning');
			$this->redirect('Sign:in');
		}
	}

}