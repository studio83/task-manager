<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette\Application\UI;
use Nette\Security as NS;
use App\Model\GroupModel;


/**
 * Class GroupPresenter
 * @package App\Presenters
 */
class GroupPresenter extends BasePresenter {

	/**
	 * @var GroupModel
	 */
	private $groupModel;


	/**
	 * GroupPresenter constructor.
	 * @param NS\User    $user
	 * @param GroupModel $groupModel
	 */
	public function __construct(NS\User $user, GroupModel $groupModel)
	{
		parent::__construct($user);
		$this->groupModel = $groupModel;
	}

	public function beforeRender()
	{
		parent::beforeRender(); // TODO: Change the autogenerated stub
	}

	public function renderDefault()
	{
		parent::renderDefault(); // TODO: Change the autogenerated stub
		$this->template->groups = $this->groupModel->getMyGroups();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentGroupForm(): UI\Form
	{
		$form = new UI\Form();
		$form->addText('name', 'Název skupiny');
		$form->addSubmit('sendForm', 'Uložit skupinu');
		$form->onSuccess[] = [$this, 'groupFormSent'];
		return $form;
	}

	/**
	 * @param UI\Form   $form
	 * @param \stdClass $values
	 */
	public function groupFormSent(UI\Form $form, \stdClass $values): void
	{
		$data = [
			'name' => $values->name
		];
		try {
			$this->groupModel->addMyGroup($data);
			$this->flashMessage('Skupina '.$values->name.' byla přidána', 'success');
		} catch(NS\AuthenticationException $e) {
			$this->flashMessage($e->getMessage(), 'danger');
		}
	}
}