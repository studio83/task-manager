<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette\Application\AbortException;
use Nette\Security as NS;
use App\Model\UserModel;
use App\Model\RegistrationModel;
use Nette\Application\UI;


/**
 * Class UserPresenter
 * @package App\Presenters
 */
class UserPresenter extends BasePresenter {

	/**
	 * @var UserModel
	 */
	private $userModel;
	/**
	 * @var RegistrationModel
	 */
	private $registrationModel;

	/**
	 * UserPresenter constructor.
	 * @param NS\User           $user
	 * @param UserModel         $userModel
	 * @param RegistrationModel $registrationModel
	 */
	public function __construct(NS\User $user, UserModel $userModel, RegistrationModel $registrationModel)
	{
		parent::__construct($user);
		$this->userModel = $userModel;
		$this->registrationModel = $registrationModel;
	}

	public function beforeRender()
	{
		parent::beforeRender();
	}

	public function renderDefault()
	{
		parent::renderDefault();
		$this->template->users = $this->userModel->getUsers();
	}

	public function renderAdd()
	{

	}

	public function renderGroup()
	{
		$this->template->groups = $this->userModel->getGroups();
	}

	/**
	 * @param int $id ID uživatele
	 * @throws NS\AuthenticationException
	 */

	public function renderEdit($id) {
		$getUser = $this->userModel->getUser($id);
		$userArray = $getUser->toArray();
		array_push($userArray, ['edit'=>true]);
		$this['userForm']->setDefaults($userArray);
	}

	/**
	 * @param int $id ID uživatele
	 * @throws AbortException
	 */

	public function actionDelete($id): void
	{
		try {
			$this->userModel->deleteUser($id);
			$this->flashMessage('Uživatel byl úspěšně vymazán', 'success');
		} catch(NS\AuthenticationException $e) {
			$this->flashMessage($e->getMessage());
		}
		$this->redirect('User:');
	}

	/**
	 * @return UI\Form
	 */

	protected function createComponentUserForm(): UI\Form
	{
		$form = new UI\Form();
		$form->addEmail('email', 'E-mail')->setRequired('Zadejte, prosím, Váš e-mail')->addRule($form::EMAIL, 'Nesprávný formát e-mailu');
		$form->addText('name', 'Jméno')->setRequired('Zadejte, prosím, své jméno');
		$form->addText('surname', 'Přijmení')->setRequired('Zadejte, prosím, své příjmení');
		$form->addSubmit('sendUserForm', 'Vytvořit uživatele');
		$form->onSuccess[] = [$this, 'userFormSent'];
		return $form;
	}

	/**
	 * @param UI\Form   $form
	 * @param \stdClass $values
	 * @throws UI\InvalidLinkException
	 * @throws AbortException
	 */

	public function userFormSent(UI\Form $form, \stdClass $values): void
	{
		$params = $this->getParameters();
		$userData = [
			'name' => $values->name,
			'surname' => $values-> surname,
			'email' => $values->email
		];
		$password = md5(uniqid());
		try {
			if($params['action'] === 'edit') {
				$this->userModel->updateUser($params['id'], $userData);
				$this->flashMessage('Uživatel '.$values->name.' '.$values->surname.' byl upraven', 'success');
			} else {
				$this->registrationModel->register($values->email, $password, $userData);
				$mailBody = '<p>Zasíláme Vám Vaši novou registaci na webu '.$this->link('//Homepage:default').'</p><p>Vaše uživatelské jméno: '.$values->email.'<br>Vaše heslo: '.$password.'</p>';
				$this->sendEmail($values->email, 'Vaše nová registrace', $mailBody);
				$this->flashMessage('Uživatel byl zaregistrován', 'success');
			}
			$this->redirect('User:');
		} catch(NS\AuthenticationException $e) {
			$this->flashMessage($e->getMessage(), 'danger');
		}
	}

	/**
	 * @return UI\Form
	 */

	protected function createComponentGroupForm(): UI\Form
	{
		$form = new UI\Form();
		$form->addText('name', 'Název skupiny');
		$form->addSubmit('sendForm', 'Uložit skupinu');
		$form->onSuccess[] = [$this, 'groupFormSent'];
		return $form;
	}

	/**
	 * @param UI\Form   $form
	 * @param \stdClass $values
	 * @throws AbortException
	 */

	public function groupFormSent(UI\Form $form, \stdClass $values): void
	{
		$formData = [
			'name' => $values->name
		];
		try {
			$this->userModel->addGroup($formData);
			$this->flashMessage('Skupina '.$values->name.' byla úspěšně přidána', 'success');
			$this->redirect('User:group');
		} catch (NS\AuthenticationException $e) {
			$this->flashMessage($e->getMessage(), 'danger');
		}
	}
}