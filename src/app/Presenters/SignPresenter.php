<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Model\AuthenticatorModel;
use Nette\Application\UI;
use Nette\Security\User;


/**
 * Class SignPresenter
 * @package App\Presenters
 */
class SignPresenter extends BasePresenter {

	/**
	 * @var AuthenticatorModel
	 */
	private $authenticator;
	/**
	 * @var User
	 */
	private $user;

	/**
	 * SignPresenter constructor.
	 * @param AuthenticatorModel $authenticator
	 * @param User               $user
	 */
	public function __construct(AuthenticatorModel $authenticator, User $user) {
		$this->authenticator = $authenticator;
		$this->user = $user;
	}

	public function beforeRender()
	{
		parent::beforeRender();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentLoginForm(): UI\Form
	{
		$form = new UI\Form();
		$form->addText('email', 'E-mail')->setRequired('Zadejte, prosím, Váš e-mail')->addRule($form::EMAIL, 'E-mail je v nesprávném formátu');
		$form->addPassword('password', 'Heslo')->setRequired('Zadejte, prosím, heslo');
		$form->addSubmit('login', 'Přihlásit');
		$form->onSuccess[] = [$this, 'loginFormSent'];
		return $form;
	}

	/**
	 * @param UI\Form   $form
	 * @param \stdClass $values
	 * @throws Nette\Application\AbortException
	 */
	public function loginFormSent(UI\Form $form, \stdClass $values): void {
		$credentials = [$values->email, $values->password];
		try {
			$this->authenticator->authenticate($credentials);
			$this->user->login(strtolower($values->email), $values->password);
			$this->flashMessage('Vaše přihlášení bylo úspěšné', 'success');
			$this->redirect('Homepage:default');
		} catch (Nette\Security\AuthenticationException $exception) {
			$this->flashMessage($exception->getMessage(), 'danger');
		}
	}

	/**
	 * @throws Nette\Application\AbortException
	 */
	public function actionOut(): void {
		$this->getUser()->logout(true);
		$this->flashMessage('Odhlášení bylo úspěšné', 'success');
		$this->redirect('Homepage:');
	}
}