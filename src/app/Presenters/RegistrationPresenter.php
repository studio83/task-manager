<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Application\UI;
use App\Model\RegistrationModel;


/**
 * Class RegistrationPresenter
 * @package App\Presenters
 */
class RegistrationPresenter extends BasePresenter {

	/**
	 * @var RegistrationModel
	 */
	private $registrationModel;

	/**
	 * RegistrationPresenter constructor.
	 * @param RegistrationModel $registrationModel
	 */
	public function __construct(RegistrationModel $registrationModel) {
		$this->registrationModel = $registrationModel;
	}

	public function renderDefault() {

	}

	/**
	 * @return UI\Form
	 */
	public function createComponentRegistrationForm(): UI\Form {
		$form = new UI\Form();
		$form->addEmail('email', 'E-mail')->setRequired('Zadejte, prosím, Váš e-mail')->addRule($form::EMAIL, 'Nesprávný formát e-mailu');
		$form->addPassword('password', 'Heslo')->setRequired('Zadejte, prosím, Vaše heslo')->addRule($form::MIN_LENGTH, 'Heslo musí mít aspoň %d znaků', '6');
		$form->addPassword('passwordVerify', 'Heslo znovu')->setRequired('Zadejte, prosím, Vaše heslo znovu pro kontrolu')->addRule($form::EQUAL, 'Hesla musí být stejná', $form['password']);
		$form->addText('name', 'Jméno')->setRequired('Zadejte, prosím, své jméno');
		$form->addText('surname', 'Přijmení')->setRequired('Zadejte, prosím, své příjmení');
		$form->addSubmit('register', 'Registrovat');
		$form->onSuccess[] = [$this, 'registrationFormSent'];
		return $form;
	}

	/**
	 * @param UI\Form   $form
	 * @param \stdClass $values
	 * @throws Nette\Application\AbortException
	 */
	public function registrationFormSent(UI\Form $form, \stdClass $values): void {
		try {
			$userData = [
				'name' => $values->name,
				'surname' => $values->surname
			];
			$this->registrationModel->register($values->email, $values->passwordVerify, $userData);
			$this->flashMessage('Registrace byla úspěšná.', 'success');
			$this->redirect('Homepage:');
		} catch(Nette\Security\AuthenticationException $e) {
			$this->flashMessage($e->getMessage(), 'danger');
		}
	}

}